#ifndef SOMMET_H_INCLUDED
#define SOMMET_H_INCLUDED
#include <stdlib.h>
#include <stdio.h>
#include <string>

class Sommet
{
public :

    std::string m_nom ;
    int influence;
    Sommet(std::string nom_sommet);
    ~Sommet();
};

#endif // SOMMET_H_INCLUDED

#ifndef GRAPHE_H_INCLUDED
#define GRAPHE_H_INCLUDED
#include "Sommet.h"
#include "Arete.h"
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <deque>

class Graphe
{
public :

    int ordre;
    int nbre_arete;

    std::vector<Arete>matrice;

    Graphe();
    ~Graphe();

    void lirefichier(int i1, int i2, int i3, int i4, Sommet s1,Sommet s2, Sommet s3, Sommet s4);
    void afficher_influences(Sommet s1,Sommet s2,Sommet s3,Sommet s4);



};

#endif // GRAPHE_H_INCLUDED

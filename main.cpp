#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include "Arete.h"
#include "Sommet.h"
#include "Graphe.h"
#include <vector>
#include <deque>

using namespace std;

int main()
{


    Graphe Test ;
    int i1, i2, i3,i4;
     // declaration des sommets (PERSONNES)
    Sommet S1 ("Matthieu") ;
    Sommet S2 ("Clara");
    Sommet S3 ("Bernard");
    Sommet S4 ("Francoise");

    Arete A1 (S1,S2);
    Arete A2 (S1,S3);
    Arete A3 (S1,S4);
    Arete A4 (S2,S3);
    Arete A5 (S2,S4);
    Arete A6 (S3,S4);

    Test.matrice.push_back(A1);
    Test.matrice.push_back(A2);
    Test.matrice.push_back(A3);
    Test.matrice.push_back(A4);
    Test.matrice.push_back(A5);
    Test.matrice.push_back(A6);


///FONCTION LECTURE DU FICHIER PLACEE DANS LE MAIN CAR LES VARIABLES NE SE PLACES PAS DANS LES BONS ATTRIBUTS
    ///LE FICHIER EST UNE MATRICE D'ADJACENCE

       ifstream fichier("influences.txt", ios::in);

    if(!fichier)
            cout << "Impossible d'ouvrir le fichier !" << endl;

    else

        fichier >>i1>>i2>>i3>>i4 ;

        S1.influence=i1;
        S2.influence=i2;                ///AFFECTATION DES INFLUENCES
        S3.influence=i3;
        S4.influence=i4;


string choix;

cout << "veuilllez rentrer le nom du fichier dont vous voulez voir les influences : (influences recommandé)"<< endl;
cin >> choix;


do
  {
    cout << "veuillez recommencer" << endl;
    cin >> choix;

}
while (choix!="influences");


    cout << "l'organisation se constitue de " << S1.m_nom << ", "<< S2.m_nom<<", "<< S3.m_nom << " et de " << S4.m_nom<<".\n\n"<<endl;


// Test.lirefichier(i1, i2, i3,i4,S1,S2,S3,S4);
    Test.afficher_influences(S1,S2,S3,S4);

    return 0;
}
